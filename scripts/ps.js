// Description:
//   Allows hubot to run commands from Slack
//
// Dependencies:
//   None
//
// Configuration:
//   HUBOT_SLACK_TOKEN as environment variable
//
// Commands:
//   hubot quit - Quit
//   hubot help - Usages
//   hubot deploy <name> <environment> <branch> - Deploy a project
//
// Author:
//   itscaro
//

var exec = require('child_process').exec
var execsync = require('child_process').execSync
var spawn = require('child_process').spawn
var carrier = require('carrier')

module.exports = function(robot) {
	robot.respond(/hi/i, function(res) {
		res.reply("Hi there")
	})

	robot.respond(/help/i, function(res) {
		res.reply(
			"hubot quit - Quit\n" +
			"hubot deploy <name> <environment> <branch> - Deploy a project\n"
		)
	})

	robot.respond(/quit/i, function(res) {
		res.reply("Quitting")

		// Ensure that the reply message is sent before quitting
		setTimeout(function() {
			process.exit()
		}, 2000)
	})

	robot.respond(/ps/i, function(res) {
		var cmd = 'ps faux | grep cap | grep deploy';

		exec(cmd, function(error, stdout, stderr) {
			res.reply("Result:")
			res.reply(stdout)
		});

		res.reply("Please wait")
        })

	robot.respond(/behat ([^ ]+) ([^ ]+)/i, function(res) {
		console.log('[Received] ', res.message.text)
//               console.log(res.message)

		var profile = res.match[1]
		var tags = res.match[2]

		var cmd = '/home/webhoster_ssh/quan/one/behat/bin/behat';
		var args = ['--profile=' + profile, '--tags=' + tags];
        var options = {
            cwd: "/home/webhoster_ssh/quan/one/behat/"
        }

        var process = spawn(cmd, args, options)

		processOut = carrier.carry(process.stdout)
		processErr = carrier.carry(process.stderr)

		processOut.on('line', function(line) {
			//res.reply(line.toString())
			console.log('stdout: ' + line.toString());
		})
		processErr.on('line', function(line) {
            //res.reply(line.toString())
            console.log('stdout: ' + line.toString());
        })
    })

	robot.respond(/deploy ([^ ]+) ([^ ]+) ([^ ]+)/i, function(res) {
        console.log('[Received] ', res.message.text)
		console.log(res.message)

		if (res.envelope.user.type == 'direct') {
			res.reply("Cannot deploy in private message")
			return
		}

        var cmd = '/usr/local/bin/cap';
		// Special case
		if (res.match[1] == 'http://programme-tv.net') {
			res.match[1] = 'programme-tv.net'
		}
        var deployName = res.match[1] + ':' + res.match[2]
		var deployBranch = res.match[3]
        var args = [deployName, 'deploy', '-S', 'branch=' + deployBranch, '-S', 'deployer="' + res.message.user.real_name + '(' + res.message.user.name + ')"'];
        var options = {
            cwd: "/home/webhoster_ssh/deploy"
        }

		if (deployBranch !== 'recette' && deployBranch !== 'recette2') {
			res.reply("You can deploy to recette or recette2 only")
            return
		}

        res.reply(":no_entry: :no_entry: :no_entry: DEPLOYING " + deployName + " " + deployBranch + " :no_entry: :no_entry: :no_entry:")
        res.reply(cmd + " " + args.toString())
        var process = spawn(cmd, args, options)

		var statusInterval = setInterval(function() {
			res.reply(cmd + " " + args.toString() + ' is still running with PID ' + process.pid)
			console.log(cmd + " " + args.toString() + ' is still runiing with PID ' + process.pid)
		}, 15000)

		var processOut = carrier.carry(process.stdout)
		var processErr = carrier.carry(process.stderr)

		processOut.on('line', function(line) {
//			res.reply(line.toString())
			console.log('stdout: ' + line.toString());
		})

		var err
		processErr.on('line', function(line) {
            err += line.toString() + "\n"
//			res.reply(line.toString())
            console.log('stderr: ' + line.toString());
        })

        process.on('exit', function (code) {
        clearInterval(statusInterval)
        res.reply(cmd + " " + args.toString() + ' exited with code ' + code.toString())
        //res.reply(err)
        if (code === 0) {
            res.reply(":white_check_mark: ==> DONE " + deployName + "@" + deployBranch + "<== :white_check_mark:")
        } else {
            res.reply(":bangbang: ==> FAILED " + deployName + "@" + deployBranch + "<== :bangbang:")
        }
            console.log("[DONE] " + deployName + "@" + deployBranch + "child process exited with code " + code.toString());
        });
    })

	robot.catchAll(function(res) {
        console.log('[Received] ', res.message.text)

		//msg.reply('pardon? :question:');
		if (res.message.text.match(new RegExp(robot.name))) {
		      res.reply('pardon? :question:');
		}
	})
}
